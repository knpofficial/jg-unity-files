﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuMotor : MonoBehaviour
{
    public void PlayGame()
    {
        StartCoroutine(LoadSceneAsync());
    }

    IEnumerator LoadSceneAsync()
    {
        AsyncOperation async = SceneManager.LoadSceneAsync("GameScene");

        while (!async.isDone)
        {
            yield return async.isDone;
        }
    }
}
