﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KartMotor : MonoBehaviour
{
    public bool Kapali
    {
        get
        {
            return _kapali;
        }

        set
        {
            _kapali = value;

            transform.localEulerAngles = (value ? Vector3.zero : new Vector3(0, 180, 0));
        }
    }
    public bool _kapali;

    public bool BenimKartim
    {
        get
        {
            return _benimKartim;
        }
        set
        {
            _benimKartim = value;

            if (value)
                this.gameObject.AddComponent<BoxCollider>();
        }
    }
    public bool _benimKartim;

    bool PistiOldu;

    int KartType(string _str)
    {
        switch (_str)
        {
            case "Club":
                return 0;
            case "Diamond":
                return 1;
            case "Heart":
                return 2;
            case "Spades":
                return 3;
        }

        return -1;
    }

    public KartType Type;

    public string Sayi;

    public void Setup()
    {
        Sayi = this.gameObject.name.Split('_')[1];
        Type = (KartType)KartType(this.gameObject.name.Split('_')[2]);
    }

    public void Bitti(bool Player)
    {
        PistiOldu = true;

        Destroy(this.gameObject);
    }

    private void OnMouseDown()
    {
        if (MasaMotor.Instance.SiraBende)
            MasaMotor.Instance.KartAt(gameObject, true);
    }



}

public enum KartType
{
    Club = 0,
    Diamond = 1,
    Heart = 2,
    Spades = 3
}
