﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MasaMotor : MonoBehaviour
{
    public static MasaMotor Instance
    {
        get
        {
            return (_instance != null ? _instance : FindObjectOfType<MasaMotor>());
        }
    }

    public bool SiraBende
    {
        get
        {
            return siraBende;
        }

        set
        {
            siraBende = value;

            if (!value)
                Invoke("BotOyna", 1f);
        }
    }
    private bool siraBende = true;

    public bool SonPistiPlayer = false;
    public bool Bitti = false;

    private static MasaMotor _instance;

    public GameObject ImgPistiGrup;
    public GameObject[] Kartlar;

    GameObject KartGetir()
    {
        List<GameObject> KarisikKartlar = new List<GameObject>();

        foreach (GameObject _kart in Kartlar)
        {
            if (!MasadakiKartlar.ContainsKey(_kart.name))
                KarisikKartlar.Add(_kart);
        }

        GameObject _return = (KarisikKartlar.Count > 0 ? KarisikKartlar[UnityEngine.Random.Range(0, KarisikKartlar.Count)] : null);

        if (_return == null && !Bitti)
            StartCoroutine(Pisti(SonPistiPlayer, null, null));

        return _return;
    }

    GameObject KartGetir(string Name)
    {
        foreach (GameObject _kart in Kartlar)
        {
            if (_kart.name == Name)
                return _kart;
        }

        return null;
    }


    public Dictionary<string, KartMotor> MasadakiKartlar = new Dictionary<string, KartMotor>();
    public Dictionary<string, KartMotor> OrtadakiKartlar = new Dictionary<string, KartMotor>();

    public Dictionary<string, KartMotor> PcKartlar = new Dictionary<string, KartMotor>();
    public Dictionary<string, KartMotor> PlayerKartlar = new Dictionary<string, KartMotor>();


    public KartMotor OrtadakiSonKart;

    public Transform Sahne;

    public int PcPuan, PlayerPuan;

    public Text TxtPisti, TxtPlayerPuan, TxtPcPuan, TxtLog;

    void Start()
    {
        #region Masanin Olusturulmasi   

        #region Kapali Kartlar

        for (int i = 0; i < 3; i++)
        {
            GameObject Prefab = KartGetir();

            GameObject Kart = Instantiate(Prefab, Sahne.Find("Merkez").transform);
            Kart.transform.localPosition = Vector3.zero;
            Kart.transform.localEulerAngles = Vector3.zero;

            Kart.transform.Translate(0.03f * (i + 1), 0, 0.1f * i);
            Kart.AddComponent<KartMotor>();
            Kart.name = Prefab.name;

            KartMotor _kartMotor = Kart.GetComponent<KartMotor>();
            _kartMotor.Kapali = true;
            _kartMotor.Setup();

            MasadakiKartlar.Add(Kart.name, _kartMotor);
            OrtadakiKartlar.Add(Kart.name, _kartMotor);
        }

        #endregion
        GameObject _prefab = KartGetir();

        GameObject AcikKart = Instantiate(_prefab, Sahne.Find("Merkez").transform);
        AcikKart.transform.localPosition = Vector3.zero;
        AcikKart.transform.localEulerAngles = Vector3.zero;

        AcikKart.transform.Translate(0, 0, -0.4f);
        AcikKart.AddComponent<KartMotor>();
        AcikKart.name = _prefab.name;

        KartMotor _motor = AcikKart.GetComponent<KartMotor>();
        _motor.Kapali = false;
        _motor.Setup();

        MasadakiKartlar.Add(AcikKart.name, _motor);
        OrtadakiKartlar.Add(AcikKart.name, _motor);

        OrtadakiSonKart = _motor;

        KartlariDagit();

        #endregion

        StartCoroutine(KartKontrol());
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
            TxtLog.gameObject.SetActive(!TxtLog.gameObject.activeInHierarchy);
    }

    public void KartlariDagit()
    {
        TxtLog.text += "#Kartlar Dagitilir" + "\n";

        #region PC Kartlari
        for (int i = 0; i < 4; i++)
        {
            GameObject Prefab = KartGetir();

            if (Prefab == null)
                return;

            GameObject Kart = Instantiate(Prefab, Sahne.Find("Ust").transform);
            Kart.transform.localPosition = Vector3.zero;
            Kart.transform.localEulerAngles = Vector3.zero;

            Kart.transform.Translate(-0.035f * i, 0, -0.1f * i);
            Kart.AddComponent<KartMotor>();
            Kart.name = Prefab.name;

            KartMotor _kartMotor = Kart.GetComponent<KartMotor>();
            _kartMotor.Kapali = true;
            _kartMotor.Setup();

            MasadakiKartlar.Add(Kart.name, _kartMotor);

            PcKartlar.Add(Kart.name, _kartMotor);
        }
        #endregion

        #region Player Kartlari
        for (int i = 0; i < 4; i++)
        {
            GameObject Prefab = KartGetir();

            if (Prefab == null)
                return;

            GameObject Kart = Instantiate(Prefab, Sahne.Find("Alt").transform);
            Kart.transform.localPosition = Vector3.zero;
            Kart.transform.localEulerAngles = Vector3.zero;

            Kart.transform.Translate(0.035f * i, 0, -0.1f * i);
            Kart.AddComponent<KartMotor>();
            Kart.name = Prefab.name;

            KartMotor _kartMotor = Kart.GetComponent<KartMotor>();
            _kartMotor.Kapali = false;
            _kartMotor.BenimKartim = true;
            _kartMotor.Setup();

            MasadakiKartlar.Add(Kart.name, _kartMotor);

            PlayerKartlar.Add(Kart.name, _kartMotor);
        }
        #endregion

    }

    public void KartAt(GameObject _game, bool BenMiyim)
    {


        GameObject Prefab = KartGetir(_game.name);

        GameObject Kart = Instantiate(Prefab, Sahne.Find("Merkez").transform);
        Kart.transform.localPosition = Vector3.zero;
        Kart.transform.localEulerAngles = Vector3.zero;

        Kart.transform.Translate(0, 0, (OrtadakiSonKart != null ? OrtadakiSonKart.transform.position.z : 0) + -0.12f);
        Kart.AddComponent<KartMotor>();
        Kart.name = Prefab.name;

        KartMotor _kartMotor = Kart.GetComponent<KartMotor>();
        _kartMotor.Kapali = false;
        _kartMotor.Setup();

        OrtadakiKartlar.Add(Kart.name, _kartMotor);

        TxtLog.text += (BenMiyim ? "#Player kart atti : " : "#Pc Kart atti : ") + _kartMotor.Type.ToString() + ": " + _kartMotor.Sayi + "\n";

        #region KartinEldenSilinmesi

        if (BenMiyim)
            PlayerKartlar.Remove(_game.name);
        else
            PcKartlar.Remove(_game.name);

        Destroy(_game);

        #endregion

        #region PistiKontrol

        if (OrtadakiSonKart != null && (OrtadakiSonKart.Sayi == _kartMotor.Sayi || _kartMotor.Sayi == "J"))
        {
            StartCoroutine(Pisti(BenMiyim, _kartMotor, OrtadakiSonKart));
        }
        else
        {
            OrtadakiSonKart = _kartMotor;
            SiraBende = !SiraBende;
        }

        #endregion

        KartlariLogla();

    }

    void BotOyna()
    {
        if (OrtadakiSonKart != null)
            foreach (KartMotor _kartim in PcKartlar.Values)
            {
                if (_kartim.Sayi == OrtadakiSonKart.Sayi)
                {
                    KartAt(_kartim.gameObject, false);
                    return;
                }
            }

        KartAt(PcKartlar.ElementAt(UnityEngine.Random.Range(0, PcKartlar.Count)).Value.gameObject, false);
    }

    void KartlariLogla()
    {
        string PcKartlari = "";

        foreach (KartMotor _kart in PcKartlar.Values)
        {
            PcKartlari += _kart.Type.ToString() + ": " + _kart.Sayi.ToString() + "|";
        }

        TxtLog.text += "-PCKartlari : " + PcKartlari + "\n";

        string PlayerKartlari = "";

        foreach (KartMotor _kart in PcKartlar.Values)
        {
            PlayerKartlari += _kart.Type.ToString() + ": " + _kart.Sayi.ToString() + "|";
        }

        TxtLog.text += "-PlayerKartlari : " + PlayerKartlari + "\n";
    }

    public IEnumerator Pisti(bool BenMiyim, KartMotor SonKart, KartMotor OrtadakiKart)
    {
        TxtLog.text += "#Pisti oldu" + "\n";

        if (SonKart == null)// Kart fazlası
        {
            Bitti = true;

            if (BenMiyim)
                PlayerPuan += 3;
            else
                PcPuan += 3;

            TxtPisti.text = "Kazanan : " + (BenMiyim ? "Player" : "Bot") + " - Puan : " + (BenMiyim ? PlayerPuan : PcPuan);
        }
        else if (SonKart.Sayi == "J" && OrtadakiKart.Sayi == "J")
        {
            if (BenMiyim)
                PlayerPuan += 20;
            else
                PcPuan += 20;
        }
        else if (SonKart.Sayi == "2" && SonKart.Type == KartType.Club)
        {
            if (BenMiyim)
                PlayerPuan += 2;
            else
                PcPuan += 2;
        }
        else if (SonKart.Sayi == "10" && SonKart.Type == KartType.Diamond)
        {
            if (BenMiyim)
                PlayerPuan += 3;
            else
                PcPuan += 3;
        }
        else if (SonKart.Sayi == "A")
        {
            if (BenMiyim)
                PlayerPuan += 1;
            else
                PcPuan += 1;
        }
        else if (SonKart.Sayi == "J")
        {
            if (BenMiyim)
                PlayerPuan += 1;
            else
                PcPuan += 1;
        }
        else
        {
            if (BenMiyim)
                PlayerPuan += 10;
            else
                PcPuan += 10;
        }

        ImgPistiGrup.SetActive(true);

        yield return new WaitForSeconds(2f);

        foreach (KartMotor _kart in OrtadakiKartlar.Values)
        {
            _kart.Bitti(BenMiyim);
        }

        OrtadakiKartlar.Clear();
        OrtadakiSonKart = null;

        SiraBende = !SiraBende;

        SonPistiPlayer = BenMiyim;

        TxtPcPuan.text = TxtPcPuan.text.Split('(')[0] + "(" + PcPuan + ")";
        TxtPlayerPuan.text = TxtPlayerPuan.text.Split('(')[0] + "(" + PlayerPuan + ")";

        if (!Bitti)
            ImgPistiGrup.SetActive(false);
        else
        {
            yield return new WaitForSeconds(2f);

            AsyncOperation async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("GameScene");

            while (!async.isDone)
            {
                yield return async.isDone;
            }
        }

    }

    IEnumerator KartKontrol()
    {
        yield return new WaitForSeconds(0.25f);

        if (Sahne.Find("Alt").childCount == 0 && Sahne.Find("Ust").childCount == 0)
            KartlariDagit();

        StartCoroutine(KartKontrol());
    }

}
